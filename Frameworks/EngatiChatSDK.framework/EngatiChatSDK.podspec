#
#  Be sure to run `pod spec lint EngatiChatSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "EngatiChatSDK"
  s.version      = "1.0.0"
  s.summary      = "iOS Chat client for engati"
  #s.description  = <<-DESC
  #                  DESC

  s.homepage     = "https://engati.com/"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.license = { :type => 'Commercial'}
  s.author             = { "Engati" => "contact@engati.com" }

  s.platform     = :ios
  s.swift_version = '4.0'
  s.ios.deployment_target = "9.0"
  s.source       = { :git => "https://gitlab.com/przala/EngatiChatSDK.git", :tag => "1.0.0" }
  s.preserve_paths = 'Frameworks/EngatiChatSDK.framework'
  s.ios.vendored_frameworks = 'Frameworks/EngatiChatSDK.framework'

  s.ios.frameworks  = 'UIKit', 'AVFoundation', 'Foundation'
  s.dependency 'Socket.IO-Client-Swift', '>= 13.1.2'
  s.dependency 'AlamofireImage', '>= 3.3'

end
